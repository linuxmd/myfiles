#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
alias sdn='shutdown now'
alias ref='sudo reflector -c US -a 12 -p https -p http --sort rate --save /etc/pacman.d/mirrorlis'
alias l='ls -a'
alias u='sudo pacman -Syu'
alias s='sudo pacman -S'
alias r='sudo pacman -Rsn'

